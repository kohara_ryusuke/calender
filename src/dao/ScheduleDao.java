package dao;

import static util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Schedule;
import exception.SQLRuntimeException;

public class ScheduleDao {

	/**
	 * insertメソッド
	 * 新しくスケジュールをDBに登録する
	 * @param connection
	 * @param schedule
	 */
	public void insert(Connection connection, Schedule schedule) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO schedules ( ");
			sql.append("    scheduleDate, ");
			sql.append("    startTime, ");
			sql.append("    endTime, ");
			sql.append("    schedule, ");
			sql.append("    scheduleMemo, ");
			sql.append("    day ");
			sql.append(") VALUES ( ");
			sql.append("    ?, ");
			sql.append("    ?, ");
			sql.append("    ?, ");
			sql.append("    ?, ");
			sql.append("    ?, ");
			sql.append("    ? ");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, schedule.getScheduleDate());
			ps.setString(2, schedule.getStartTime());
			ps.setString(3, schedule.getEndTime());
			ps.setString(4, schedule.getSchedule());
			ps.setString(5, schedule.getScheduleMemo());
			ps.setInt(6, schedule.getDay());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	 * updateメソッド
	 * 既に登録されているスケジュールを変更する
	 * @param connection
	 * @param schedule
	 */
	public void update(Connection connection, Schedule schedule) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE schedules SET ");
			sql.append("    scheduleDate = ?, ");
			sql.append("    startTime = ?, ");
			sql.append("    endTime = ?, ");
			sql.append("    schedule = ?, ");
			sql.append("    scheduleMemo = ?, ");
			sql.append("    day = ?");
			sql.append("    WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, schedule.getScheduleDate());
			ps.setString(2, schedule.getStartTime());
			ps.setString(3, schedule.getEndTime());
			ps.setString(4, schedule.getSchedule());
			ps.setString(5, schedule.getScheduleMemo());
			ps.setInt(6, schedule.getDay());
			ps.setInt(7, schedule.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	 * deleteメソッド
	 * 登録されているスケジュールを削除する
	 * @param connection
	 * @param id 数値
	 */
	public void delete(Connection connection, int id) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM schedules WHERE id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	 * selectメソッド
	 * 日付の条件に合うデータを取得する
	 * @param date 日付
	 * @param connection
	 * @param num 数値
	 * @return List<Schedule>
	 */
	public List<Schedule> select(String date, Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM schedules WHERE");
			sql.append("    scheduleDate LIKE ? ");
			sql.append("ORDER BY startTime ASC limit " + num);

			date = date + "%";
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, date);
			ResultSet rs = ps.executeQuery();
			List<Schedule> schedules = toSchedules(rs);
			return schedules;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	 * selectメソッド
	 * 指定されたid以外で日付の条件に合うデータを取得する
	 * @param date 日付
	 * @param id 数値
	 * @param connection
	 * @param num
	 * @return List<Schedule>
	 */
	public List<Schedule> select(String date, int id, Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM schedules WHERE");
			sql.append("    scheduleDate LIKE ? ");
			sql.append("    AND id != ? ");
			sql.append("ORDER BY startTime ASC limit " + num);

			date = date + "%";
			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, date);
			ps.setInt(2, id);
			ResultSet rs = ps.executeQuery();
			List<Schedule> schedules = toSchedules(rs);
			return schedules;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	 * selectメソッド
	 * 指定されたidのデータを取得する
	 * @param scheduleId 数値
	 * @param connection
	 * @param num
	 * @return List<Schedule>
	 */
	public Schedule select(int scheduleId,  Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM schedules WHERE id= ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, scheduleId);
			ResultSet rs = ps.executeQuery();
			Schedule schedule = toSchedule(rs);
			return schedule;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Schedule> toSchedules(ResultSet rs) throws SQLException {

		List<Schedule> schedules = new ArrayList<Schedule>();
		try {
			while (rs.next()) {
				Schedule schedule = new Schedule();
				schedule.setId(rs.getInt("id"));
				schedule.setScheduleDate(rs.getString("scheduleDate"));
				schedule.setStartTime(rs.getString("startTime").substring(0,5));
				schedule.setEndTime(rs.getString("endTime").substring(0,5));
				schedule.setSchedule(rs.getString("schedule"));
				schedule.setScheduleMemo(rs.getString("scheduleMemo"));
				schedule.setDay(rs.getInt("day"));
				schedules.add(schedule);
			}
			return schedules;
		} finally {
			close(rs);
		}
	}

	private Schedule toSchedule(ResultSet rs) throws SQLException {

		Schedule schedule = new Schedule();
		try {
			while (rs.next()) {
				schedule.setId(rs.getInt("id"));
				schedule.setScheduleDate(rs.getString("scheduleDate"));
				schedule.setStartTime(rs.getString("startTime").substring(0,5));
				schedule.setEndTime(rs.getString("endTime").substring(0,5));
				schedule.setSchedule(rs.getString("schedule"));
				schedule.setScheduleMemo(rs.getString("scheduleMemo"));
				schedule.setDay(rs.getInt("day"));
			}
			return schedule;
		} finally {
			close(rs);
		}
	}
}
