package service;

import static util.CloseableUtil.*;
import static util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Schedule;
import dao.ScheduleDao;

public class ScheduleService {

	/**
	 * insertメソッド
	 * パラメータをdaoに渡す
	 * @param schedule
	 */
	public void insert(Schedule schedule) {
		Connection connection = null;
		try {
			connection = getConnection();
			new ScheduleDao().insert(connection,schedule);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * updateメソッド
	 * パラメータをdaoに渡す
	 * @param schedule
	 */
	public void update(Schedule schedule) {
		Connection connection = null;
		try {
			connection = getConnection();
			new ScheduleDao().update(connection,schedule);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * deleteメソッド
	 * パラメータをdaoに渡す
	 * @param schedule
	 */
	public void delete(int id) {
		Connection connection = null;
		try {
			connection = getConnection();
			new ScheduleDao().delete(connection,id);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * selectメソッド
	 * 年のyearと月のmonthをくっつけ、一つの日時にし、daoに渡す
	 * @param year
	 * @param month
	 * @return List<Schedule>
	 */
	public List<Schedule> select(int year, int month) {
		final int LIMIT_NUM = 1000;
		String date = new String();
		if(month < 10) {
			date = year + "-0" + month;
		}else {
			date = year + "-" + month;
		}

		Connection connection = null;
		try {
			connection = getConnection();
			List<Schedule> schedules = new ScheduleDao().select(date, connection, LIMIT_NUM);
			commit(connection);

			return schedules;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * selectメソッド
	 * 年のyearと月のmonth,日のdayをくっつけ、一つの日時にし、daoに渡す
	 * @param year
	 * @param month
	 * @return List<Schedule>
	 */
	public List<Schedule> select(int year, int month, int day) {
		final int LIMIT_NUM = 1000;
		String date = new String();
		if(month < 10) {
			if(day < 10) {
				date = year + "-0" + month + "-0" + day;
			}else {
				date = year + "-0" + month + "-" + day;
			}
		}else {
			if(day < 10) {
				date = year + "-" + month + "-0" + day;
			}else {
				date = year + "-" + month + "-" + day;
			}
		}

		Connection connection = null;
		try {
			connection = getConnection();
			List<Schedule> schedules = new ScheduleDao().select(date, connection, LIMIT_NUM);
			commit(connection);

			return schedules;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * selectメソッド
	 * 年のyearと月のmonth,日のdayをくっつけ、一つの日時にし、idと共にdaoに渡す
	 * @param year
	 * @param month
	 * @return List<Schedule>
	 */
	public List<Schedule> select(int year, int month, int day, int id) {
		final int LIMIT_NUM = 1000;
		String date = new String();
		if(month < 10) {
			if(day < 10) {
				date = year + "-0" + month + "-0" + day;
			}else {
				date = year + "-0" + month + "-" + day;
			}
		}else {
			if(day < 10) {
				date = year + "-" + month + "-0" + day;
			}else {
				date = year + "-" + month + "-" + day;
			}
		}

		Connection connection = null;
		try {
			connection = getConnection();
			List<Schedule> schedules = new ScheduleDao().select(date, id, connection, LIMIT_NUM);
			commit(connection);

			return schedules;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	 * selectメソッド
	 * 指定されたidをdaoに渡す
	 * @param year
	 * @param month
	 * @return List<Schedule>
	 */
	public Schedule select(int scheduleId) {

		Connection connection = null;
		try {
			connection = getConnection();
			Schedule schedule = new ScheduleDao().select(scheduleId, connection);
			commit(connection);

			return schedule;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


}
