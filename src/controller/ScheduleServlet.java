package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Schedule;
import service.ScheduleService;

@WebServlet(urlPatterns = { "/schedule" })
public class ScheduleServlet extends HttpServlet{

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException{

		HttpSession session = request.getSession();
		int year = 0;
		if(session.getAttribute("year") != null) {
			year = (int) session.getAttribute("year");
		} else {
			year = Integer.parseInt(request.getParameter("year"));
		}
		int month = 0;
		if(session.getAttribute("month") != null) {
			month = (int) session.getAttribute("month");
		} else {
			month = Integer.parseInt(request.getParameter("month"));
		}
        int day = 0;
        if(session.getAttribute("day") != null) {
			day = (int) session.getAttribute("day");
		} else {
			day = Integer.parseInt(request.getParameter("day"));
		}

        if(session.getAttribute("errorMessages") != null) {
        	List<String> errorMessages = new ArrayList<String>();
        	errorMessages = (List<String>) (session.getAttribute("errorMessages"));
        	request.setAttribute("errorMessages",  errorMessages);
        }
        session.invalidate();

        int monthLastDay = getMonthLastDay(year, month);

        List<Integer> yearList = new ArrayList<>();
        for (int i = 2015 ; i <= 2025 ; i++){
            yearList.add(i);
        }
        List<Integer> monthList = new ArrayList<>();
        for (int i = 1 ; i <= 12 ; i++){
            monthList.add(i);
        }
        List<Integer> dayList = new ArrayList<>();
        for (int i = 1 ; i <= monthLastDay ; i++){
            dayList.add(i);
        }
        List<Integer> hourList = new ArrayList<>();
        for (int i = 0 ; i <= 23 ; i++){
            hourList.add(i);
        }

        List<Schedule> schedules = new ScheduleService().select(year, month, day);
        if(schedules.size() == 0) {
        	schedules = null;
        }

        String[] scheduleArray = new String[48];
        int[] heightArray = new int[48];
        int[] hourArray = new int[48];
        int[] idArray = new int[48];
        for (int i = 0 ; i < 48 ; i++){
            scheduleArray[i] ="";
        	heightArray[i] = 0;
        	hourArray[i] = i;
        	idArray[i] = 0;
        }

        if(schedules != null) {
        	for(int i= 0; i < schedules.size(); i++) {
        		String totalTime = new String();
        		String schedule = schedules.get(i).getSchedule();
        		int startHour = Integer.parseInt(((String)schedules.get(i).getStartTime()).substring(0,2));
        		int start = startHour * 2;
        		int startMinute = Integer.parseInt(((String)schedules.get(i).getStartTime()).substring(3,5));
        		if(startMinute == 30) {
        			start++;
        		}
        		if(heightArray[start] == 0) {
        			int endHour = Integer.parseInt(((String)schedules.get(i).getEndTime()).substring(0,2));
        			int end = (endHour - startHour) * 2;
        			int endMinute = Integer.parseInt(((String)schedules.get(i).getEndTime()).substring(3,5));
        			if(startMinute == 30) {
        				end--;
        			}
        			if(endMinute == 30) {
        				end++;
        			}
        			if(startMinute == 30 && endMinute == 30) {
        				totalTime = startHour + ":" + startMinute + "-" + endHour + ":" + endMinute + " ";
        			} else if(startMinute == 30) {
        				totalTime = startHour + ":" + startMinute + "-" + endHour + ":00 ";
        			} else if(endMinute == 30) {
        				totalTime = startHour + ":00-" + endHour + ":" + endMinute + " ";
        			} else {
        				totalTime = startHour + ":00-" + endHour + ":00 ";
        			}
        			scheduleArray[start] = totalTime + schedule;
        			heightArray[start] = end;
        			idArray[start] = schedules.get(i).getId();

        			for (int j = 1 ; j < end ; j++){
        				heightArray[start + j] = -1;
        			}
        		}
        	}
        }

        request.setAttribute("year", year);
        request.setAttribute("month", month);
        request.setAttribute("day", day);
        request.setAttribute("yearList", yearList);
        request.setAttribute("monthList", monthList);
        request.setAttribute("dayList", dayList);
        request.setAttribute("hourList", hourList);
        request.setAttribute("heightArray", heightArray);
        request.setAttribute("scheduleArray", scheduleArray);
        request.setAttribute("hourArray", hourArray);
        request.setAttribute("idArray", idArray);
        request.getRequestDispatcher("schedule.jsp").forward(request, response);
    }

	private int getMonthLastDay(int year, int month) {
		Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 0);
        int thisMonthlastDay = calendar.get(Calendar.DATE);
        return thisMonthlastDay;
	}
}
