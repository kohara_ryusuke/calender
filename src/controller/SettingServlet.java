package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Schedule;
import service.ScheduleService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet{

	@Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException{

		int scheduleId = 0;
		HttpSession session = request.getSession();
		if(session.getAttribute("id") != null) {
			scheduleId = (int) session.getAttribute("id");
		} else {
			scheduleId  = Integer.parseInt(request.getParameter("scheduleId"));
		}
		if(session.getAttribute("errorMessages") != null) {
        	List<String> errorMessages = new ArrayList<String>();
        	errorMessages = (List<String>) (session.getAttribute("errorMessages"));
        	request.setAttribute("errorMessages",  errorMessages);
        }
		session.invalidate();

		Schedule schedule = new ScheduleService().select(scheduleId);
		String date = schedule.getScheduleDate();
		int year = Integer.parseInt(date.substring(0,4));
		int month = Integer.parseInt(date.substring(5,7));
		int day = schedule.getDay();
		int monthLastDay = getMonthLastDay(year, month);
		String plan = schedule.getSchedule();
		String memo = schedule.getScheduleMemo();
		int startHour = Integer.parseInt(((String)schedule.getStartTime()).substring(0,2));
    	int startMinute = Integer.parseInt(((String)schedule.getStartTime()).substring(3,5));
    	int endHour = Integer.parseInt(((String)schedule.getEndTime()).substring(0,2));
    	int endMinute = Integer.parseInt(((String)schedule.getEndTime()).substring(3,5));

		List<Schedule> schedules = new ScheduleService().select(year, month, day);
        if(schedules.size() == 0) {
        	schedules = null;
        }
        String[] scheduleArray = new String[48];
        int[] heightArray = new int[48];
        int[] hourArray = new int[48];
        int[] idArray = new int[48];
        for (int i = 0 ; i < 48 ; i++){
            scheduleArray[i] ="";
        	heightArray[i] = 0;
        	hourArray[i] = i;
        	idArray[i] = 0;
        }
        if(schedules != null) {
        	for(int i= 0; i < schedules.size(); i++) {
        		String totalTime = new String();
        		String setPlan = schedules.get(i).getSchedule();
        		int sHour = Integer.parseInt(((String)schedules.get(i).getStartTime()).substring(0,2));
        		int start = sHour * 2;
        		int sMinute = Integer.parseInt(((String)schedules.get(i).getStartTime()).substring(3,5));
        		if(sMinute == 30) {
        			start++;
        		}
        		if(heightArray[start] == 0) {
        			int eHour = Integer.parseInt(((String)schedules.get(i).getEndTime()).substring(0,2));
        			int end = (eHour - sHour) * 2;
        			int eMinute = Integer.parseInt(((String)schedules.get(i).getEndTime()).substring(3,5));
        			if(sMinute == 30) {
        				end--;
        			}
        			if(eMinute == 30) {
        				end++;
        			}
        			if(sMinute == 30 && eMinute == 30) {
        				totalTime = sHour + ":" + sMinute + "-" + eHour + ":" + eMinute + " ";
        			} else if(sMinute == 30) {
        				totalTime = sHour + ":" + sMinute + "-" + eHour + ":00 ";
        			} else if(eMinute == 30) {
        				totalTime = sHour + ":00-" + eHour + ":" + eMinute + " ";
        			} else {
        				totalTime = sHour + ":00-" + eHour + ":00 ";
        			}
        			scheduleArray[start] = totalTime + setPlan;
        			heightArray[start] = end;
        			if(schedules.get(i).getId() == scheduleId) {
        				idArray[start] = 1;
        			}
        			for (int j = 1 ; j < end ; j++){
        				heightArray[start + j] = -1;
        			}
        		}
        	}
        }

        List<Integer> yearList = new ArrayList<>();
        for (int i = 2015 ; i <= 2025 ; i++){
            yearList.add(i);
        }
        List<Integer> monthList = new ArrayList<>();
        for (int i = 1 ; i <= 12 ; i++){
            monthList.add(i);
        }
        List<Integer> dayList = new ArrayList<>();
        for (int i = 1 ; i <= monthLastDay ; i++){
            dayList.add(i);
        }
        List<Integer> hourList = new ArrayList<>();
        for (int i = 0 ; i <= 23 ; i++){
            hourList.add(i);
        }

    	request.setAttribute("id",  scheduleId);
        request.setAttribute("year", year);
        request.setAttribute("month", month);
        request.setAttribute("day", day);
        request.setAttribute("plan", plan);
        request.setAttribute("memo", memo);
        request.setAttribute("yearList", yearList);
        request.setAttribute("monthList", monthList);
        request.setAttribute("dayList", dayList);
        request.setAttribute("hourList", hourList);
        request.setAttribute("startHour", startHour);
        request.setAttribute("startMinute", startMinute);
        request.setAttribute("endHour", endHour);
        request.setAttribute("endMinute", endMinute);
        request.setAttribute("heightArray", heightArray);
        request.setAttribute("scheduleArray", scheduleArray);
        request.setAttribute("hourArray", hourArray);
        request.setAttribute("idArray", idArray);
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException{

		HttpSession session = request.getSession();

		int id = Integer.parseInt(request.getParameter("id"));
		int year = Integer.parseInt(request.getParameter("selectYear"));
		int month = Integer.parseInt(request.getParameter("selectMonth"));
		int day = Integer.parseInt(request.getParameter("selectDay"));
		int startHour = Integer.parseInt(request.getParameter("startHour"));
		int startMinute = Integer.parseInt(request.getParameter("startMinute"));
		int endHour = Integer.parseInt(request.getParameter("endHour"));
		int endMinute = Integer.parseInt(request.getParameter("endMinute"));
		String plan = request.getParameter("plan");
		String memo = request.getParameter("memo");

		Schedule schedule = new ScheduleService().select(id);
		String date = schedule.getScheduleDate();
		int beforeYear = Integer.parseInt(date.substring(0,4));
		int beforeMonth = Integer.parseInt(date.substring(5,7));
		int beforeDay = schedule.getDay();
		String beforeStartTime = schedule.getStartTime();
		String startTime = new String();
		if(startMinute == 30 && startHour >= 10) {
			startTime = startHour + ":" + startMinute ;
		} else if(startMinute == 30) {
			startTime = "0" + startHour + ":" + startMinute;
		} else {
			startTime = "0" + startHour + ":00";
		}
		String beforeEndTime = schedule.getEndTime();
		String endTime = new String();
		if(endMinute == 30 && endHour >= 10) {
			endTime = endHour + ":" + endMinute ;
		} else if(endMinute == 30) {
			endTime = "0" + endHour + ":" + endMinute;
		} else {
			endTime = "0" + endHour + ":00";
		}
		String beforePlan = schedule.getSchedule();
		String beforeMemo = schedule.getScheduleMemo();
		if((year == beforeYear) && (month == beforeMonth) && (day == beforeDay) && (startTime.equals(beforeStartTime))
				&& (endTime.equals(beforeEndTime)) && (plan.equals(beforePlan)) && (memo.equals(beforeMemo))){
			session.setAttribute("selectYear", year);
			session.setAttribute("selectMonth", month);
			response.sendRedirect("./top");
			return;
		}

		List<String> errorMessages = new ArrayList<String>();
		boolean error = false;
		List<Schedule> schedules = new ScheduleService().select(year, month, day, id);
		for(int i = 0; i < schedules.size(); i++) {
			int setStartHour = Integer.parseInt(((String)schedules.get(i).getStartTime()).substring(0,2));
			int setStartMinute = Integer.parseInt(((String)schedules.get(i).getStartTime()).substring(3,5));
			int setEndHour = Integer.parseInt(((String)schedules.get(i).getEndTime()).substring(0,2));
			int setEndMinute = Integer.parseInt(((String)schedules.get(i).getEndTime()).substring(3,5));

			SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
			try {
				Date st = parser.parse(setStartHour + ":" + setStartMinute);
				Date en = parser.parse(setEndHour + ":" + setEndMinute);
				Date ss = parser.parse(startHour + ":" + startMinute);
				Date ee = parser.parse(endHour + ":" + endMinute);
			    if (ss.after(st) && ss.before(en)) {
			    	error = true;
			    }
			    if (ee.after(st) && ee.before(en)) {
			    	error = true;
			    }
			    if((ss.equals(st) || ss.before(st)) && (ee.after(en) || ee.equals(en))) {
			    	error = true;
			    }
			} catch (ParseException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}
			if(error) {
				errorMessages.add("時刻が重複しています");
				session.setAttribute("id", id);
				session.setAttribute("errorMessages", errorMessages);
				response.sendRedirect("./setting");
				return;
			}
		}

		Schedule newSchedule = new Schedule();
		newSchedule.setId(id);
		newSchedule.setScheduleDate(year + "-" + month + "-" + day);
		newSchedule.setStartTime(startHour + ":" + startMinute);
		newSchedule.setEndTime(endHour + ":" + endMinute);
		newSchedule.setSchedule(plan);
		newSchedule.setScheduleMemo(memo);
		newSchedule.setDay(day);

		new ScheduleService().update(newSchedule);

		session.setAttribute("selectYear", year);
		session.setAttribute("selectMonth", month);
		response.sendRedirect("./top");
	}

	private int getMonthLastDay(int year, int month) {
		Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, 0);
        int thisMonthlastDay = calendar.get(Calendar.DATE);
        return thisMonthlastDay;
	}
}

