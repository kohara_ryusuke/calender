package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Day;
import beans.Schedule;
import service.ScheduleService;

@WebServlet(urlPatterns = { "/top" })
public class TopServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		int[] calendarDay = new int[42];
		int count = 0;
		Calendar calendar = Calendar.getInstance();
		int year = 0;
		if(session.getAttribute("selectYear") != null) {
			year = (int) session.getAttribute("selectYear");
		}else if(request.getParameter("year") != null){
			year = Integer.parseInt((String)request.getParameter("year"));
		} else{
			year = calendar.get(Calendar.YEAR);
		}

		int month = 0;
		if(session.getAttribute("selectMonth") != null) {
			month = ((int) session.getAttribute("selectMonth")) - 1;
		}else if(request.getParameter("month") != null){
			month = Integer.parseInt((String)request.getParameter("month")) - 1;
		}else {
			month = calendar.get(Calendar.MONTH);
		}
		session.invalidate();

		int day = calendar.get(Calendar.DATE);
        /* 日付取得 */
        count = setDateArray(year, month, calendarDay, count);
        /* 何週あるか */
        int weekCount = count / 7;

        List<Day> oneWeek = new ArrayList<Day>();
        List<List<Day>> allWeek = new ArrayList<List<Day>>();

        for (int i = 0 ; i < weekCount ; i++){
            for (int j = i * 7 ; j < i * 7 + 7 ; j++){
            	Day dayMonth = new Day();
                if (calendarDay[j] > 35){
                	dayMonth.setDay(calendarDay[j] - 35);
                	/* 当月ではないところはsetThisMonthをfalseに */
                    dayMonth.setThisMonth(false);
                    oneWeek.add(dayMonth);
                }else{
                    dayMonth.setDay(calendarDay[j]);
                    /* 当月のところはtrueに */
                    dayMonth.setThisMonth(true);
                    oneWeek.add(dayMonth);
                }
            }
            allWeek.add(oneWeek);
            oneWeek = new ArrayList<Day>();
        }

        /* 実際の月の数字に合わせる */
        month += 1;

        List<Schedule> schedules = new ScheduleService().select(year, month);

        request.setAttribute("year", year);
        request.setAttribute("month",  month);
        request.setAttribute("day", day);
        request.setAttribute("allWeek",allWeek);
        request.setAttribute("schedules", schedules);
		request.getRequestDispatcher("top.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int newMonth = 0;
		if(!(request.getParameter("lastMonth") == null)) {
			/* Calendarの月は0～11で表すのでそれに合わせる */
			newMonth = Integer.parseInt(request.getParameter("lastMonth")) - 2;
		} else if(!(request.getParameter("nextMonth") == null)) {
			newMonth = Integer.parseInt(request.getParameter("nextMonth"));
		}

		int[] calendarDay = new int[42];
		int count = 0;
		Calendar calendar = Calendar.getInstance();
		int year = Integer.parseInt(request.getParameter("year"));
		int month = newMonth;
        int day = calendar.get(Calendar.DATE);
        count = setDateArray(year, month, calendarDay, count);
        int weekCount = count / 7;

        List<Day> oneWeek = new ArrayList<Day>();
        List<List<Day>> allWeek = new ArrayList<List<Day>>();

        for (int i = 0 ; i < weekCount ; i++){
            for (int j = i * 7 ; j < i * 7 + 7 ; j++){
            	Day dayMonth = new Day();
                if (calendarDay[j] > 35){
                	dayMonth.setDay(calendarDay[j] - 35);
                    dayMonth.setThisMonth(false);
                    oneWeek.add(dayMonth);
                }else{
                    dayMonth.setDay(calendarDay[j]);
                    dayMonth.setThisMonth(true);
                    oneWeek.add(dayMonth);
                }
            }
            allWeek.add(oneWeek);
            oneWeek = new ArrayList<Day>();
        }

        month += 1;
        /* monthが1より小さくなったら12に、yearを1つ戻す*/
        if(month < 1) {
        	month = 12;
        	year -= 1;
        }
        /* monthが12を超えたら1にし、yearを1増やす */
        if(month > 12) {
        	month = 1;
        	year += 1;
        }

        List<Schedule> schedules = new ScheduleService().select(year, month);

        request.setAttribute("year", year);
        request.setAttribute("month",  month);
        request.setAttribute("day", day);
        request.setAttribute("allWeek",allWeek);
        request.setAttribute("schedules", schedules);
		request.getRequestDispatcher("top.jsp").forward(request, response);

	}

	 protected int setDateArray(int year, int month, int[] calendarDay, int count){

	        Calendar calendar = Calendar.getInstance();

	        /* 今月が何曜日から開始されているか確認する */
	        calendar.set(year, month, 1);
	        int startWeek = calendar.get(Calendar.DAY_OF_WEEK);

	        /* 先月が何日までだったかを確認する(0日目＝先月の最終日) */
	        calendar.set(year, month, 0);
	        int beforeMonthlastDay = calendar.get(Calendar.DATE);

	        /* 今月が何日までかを確認する */
	        calendar.set(year, month + 1, 0);
	        int thisMonthlastDay = calendar.get(Calendar.DATE);

	        /* 先月分の日付を格納する */
	        for (int i = startWeek - 2 ; i >= 0 ; i--){
	            calendarDay[count++] = beforeMonthlastDay - i + 35;
	        }

	        /* 今月分の日付を格納する */
	        for (int i = 1 ; i <= thisMonthlastDay ; i++){
	            calendarDay[count++] = i;
	        }

	        /* 翌月分の日付を格納する */
	        int nextMonthDay = 1;
	        while (count % 7 != 0){
	            calendarDay[count++] = 35 + nextMonthDay++;
	        }

	        return count;
	    }
}
