package controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Schedule;
import service.ScheduleService;

@WebServlet(urlPatterns = { "/scheduleInsert" })
public class ScheduleInsertServlet extends HttpServlet{

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{

		int year = Integer.parseInt(request.getParameter("selectYear"));
		int month = Integer.parseInt(request.getParameter("selectMonth"));
		int day = Integer.parseInt(request.getParameter("selectDay"));
		int startHour = Integer.parseInt(request.getParameter("startHour"));
		int startMinute = Integer.parseInt(request.getParameter("startMinute"));
		int endHour = Integer.parseInt(request.getParameter("endHour"));
		int endMinute = Integer.parseInt(request.getParameter("endMinute"));
		String plan = request.getParameter("plan");
		String memo = request.getParameter("memo");

		List<String> errorMessages = new ArrayList<String>();
		HttpSession session = request.getSession();

		List<Schedule> schedules = new ScheduleService().select(year, month, day);
		for(int i = 0; i < schedules.size(); i++) {
			int setStartHour = Integer.parseInt(((String)schedules.get(i).getStartTime()).substring(0,2));
			int setStartMinute = Integer.parseInt(((String)schedules.get(i).getStartTime()).substring(3,5));
			int setEndHour = Integer.parseInt(((String)schedules.get(i).getEndTime()).substring(0,2));
			int setEndMinute = Integer.parseInt(((String)schedules.get(i).getEndTime()).substring(3,5));
			boolean error = false;

			SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
			try {
				Date st = parser.parse(setStartHour + ":" + setStartMinute);
				Date en = parser.parse(setEndHour + ":" + setEndMinute);
				Date ss = parser.parse(startHour + ":" + startMinute);
				Date ee = parser.parse(endHour + ":" + endMinute);
			    if (ss.after(st) && ss.before(en)) {
			    	error = true;
			    }
			    if (ee.after(st) && ee.before(en)) {
			    	error = true;
			    }
			    if((ss.equals(st) || ss.before(st)) && (ee.after(en) || ee.equals(en))) {
			    	error = true;
			    }
			} catch (ParseException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			if(error) {
				errorMessages.add("時刻が重複しています");
				session.setAttribute("year", year);
				session.setAttribute("month", month);
				session.setAttribute("day", day);
				session.setAttribute("errorMessages", errorMessages);
				response.sendRedirect("./schedule");
				return;
			}
		}

		Schedule schedule = new Schedule();
		schedule.setScheduleDate(year + "-" + month + "-" + day);
		schedule.setStartTime(startHour + ":" + startMinute);
		schedule.setEndTime(endHour + ":" + endMinute);
		schedule.setSchedule(plan);
		schedule.setScheduleMemo(memo);
		schedule.setDay(day);

		new ScheduleService().insert(schedule);

		session.setAttribute("selectYear", year);
		session.setAttribute("selectMonth", month);
		response.sendRedirect("./top");
	}
}
