package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import service.ScheduleService;

@WebServlet(urlPatterns = { "/delete" })
public class ScheduleDelete extends HttpServlet{

	@Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException{

		int id = Integer.parseInt(request.getParameter("id"));
		int year = Integer.parseInt(request.getParameter("year"));
		int month = Integer.parseInt(request.getParameter("month"));

		new ScheduleService().delete(id);

		HttpSession session = request.getSession();
		session.setAttribute("selectYear", year);
		session.setAttribute("selectMonth", month);
		response.sendRedirect("./top");
	}

}
