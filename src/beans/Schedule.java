package beans;

public class Schedule {
	private int id;
	private String scheduleDate;
	private String startTime;
	private String endTime;
	private String schedule;
	private String scheduleMemo;
	private int day;

	/**
	 * getIdメソッド
	 * @return idとして数値
	 */
	public int getId() {
		return id;
	}
	/**
	 * setIdメソッド
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * getScheduleDateメソッド
	 * @return scheduleDateとして文字列
	 */
	public String getScheduleDate() {
		return scheduleDate;
	}
	/**
	 * setScheduleDateメソッド
	 * @param scheduleDate
	 */
	public void setScheduleDate(String scheduleDate) {
		this.scheduleDate = scheduleDate;
	}
	/**
	 * getStartTimeeメソッド
	 * @return startTimeとして文字列
	 */
	public String getStartTime() {
		return startTime;
	}
	/**
	 * setStartTimeメソッド
	 * @param startTime
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	/**
	 * getEndTimeeメソッド
	 * @return endTimeとして文字列
	 */
	public String getEndTime() {
		return endTime;
	}
	/**
	 * setEndTimeメソッド
	 * @param endTime
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	/**
	 * getScheduleメソッド
	 * @return scheduleとして文字列
	 */
	public String getSchedule() {
		return schedule;
	}
	/**
	 * setScheduleメソッド
	 * @param schedule
	 */
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}
	/**
	 * getScheduleMemoメソッド
	 * @return scheduleMemoとして文字列
	 */
	public String getScheduleMemo() {
		return scheduleMemo;
	}
	/**
	 * setScheduleMemoメソッド
	 * @param scheduleMemo
	 */
	public void setScheduleMemo(String scheduleMemo) {
		this.scheduleMemo = scheduleMemo;
	}
	/**
	 * getDayメソッド
	 * @return dayとして数値
	 */
	public int getDay() {
		return day;
	}
	/**
	 * setDayメソッド
	 * @param day
	 */
	public void setDay(int day) {
		this.day = day;
	}
}
