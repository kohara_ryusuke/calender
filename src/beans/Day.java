package beans;

public class Day {
	private int day;
	private boolean thisMonth;

	/**
	 * getDayメソッド
	 * @return dayとして数値
	 */
	public int getDay() {
		return day;
	}
	/**
	 * setDayメソッド
	 * @param day
	 */
	public void setDay(int day) {
		this.day = day;
	}
	/**
	 * isThisMonthメソッド
	 * @return thisMonthとしてtrueかfalse
	 */
	public boolean isThisMonth() {
		return thisMonth;
	}
	/**
	 * setThisMonthメソッド
	 * @param thisMonth
	 */
	public void setThisMonth(boolean thisMonth) {
		this.thisMonth = thisMonth;
	}

}
