<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" Content="text/html;charset=Shift_JIS">
<title>スケジュール登録</title>
<link href="./css/schedule.css" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>
<body>
	<form method="get" action="./top">
		<input type="hidden" value="${month}" name="month">
		<input type="hidden" value="${year}" name="year">
		<input type="submit" name="register" value="カレンダーに戻る">
	</form>
	<h1>スケジュール登録</h1>

	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
	</c:if>

	<div id="contents">
		<div id="left">
			<table class="sche">
				<tr><td class="top" style="width:80px">時刻</td><td class="top" style="width:300px">予定</td></tr>

				<c:forEach items="${hourArray}" var="hour">
					<c:if test="${hour % 2 == 0}">
					<tr>
					<td class="time"><fmt:formatNumber value="${hour / 2}" maxFractionDigits="0"/>:00</td>
						<c:if test="${heightArray[hour] != 0}">
							<c:if test="${heightArray[hour] != -1}">
								<td class="ex" rowspan="${heightArray[hour]}" ><a href="./setting?scheduleId=${idArray[hour]}">${scheduleArray[hour]}</a></td>
							</c:if>
						</c:if>
						<c:if test="${heightArray[hour] == 0}">
							<td class="contents"></td>
						</c:if>
					</tr>
					</c:if>
					<c:if test="${hour % 2 != 0}">
					<tr>
					<td class="timeb"></td>
						<c:if test="${heightArray[hour] != 0}">
							<c:if test="${heightArray[hour] != -1}">
									<td class="ex" rowspan="${heightArray[hour]}" ><a href="./setting?scheduleId=${idArray[hour]}">${scheduleArray[hour]}</a></td>
							</c:if>
						</c:if>
						<c:if test="${heightArray[hour] == 0}">
							<td class="contentsb"></td>
						</c:if>
					</tr>
					</c:if>
				</c:forEach>
			</table>
		</div>
		<div id="right">
			<form method="post" action="./scheduleInsert" onSubmit="return message()">
				<table>
					<tr>
						<td nowrap>日付</td>
						<td>
						<select name="selectYear">
							<c:forEach items="${yearList}" var="oneYear">
								<c:if test="${oneYear == year}">
									<option value="${oneYear}" selected>${oneYear}年</option>
								</c:if>
								<c:if test="${oneYear != year}">
									<option value="${oneYear}">${oneYear}年</option>
								</c:if>
							</c:forEach>
						</select>
						<select name="selectMonth">
							<c:forEach items="${monthList}" var="oneMonth">
								<c:if test="${oneMonth == month}">
									<option value="${oneMonth}" selected>${oneMonth}月</option>
								</c:if>
								<c:if test="${oneMonth != month}">
									<option value="${oneMonth}">${oneMonth}月</option>
								</c:if>
							</c:forEach>
						</select>
						<select name="selectDay">
							<c:forEach items="${dayList}" var="oneDay">
								<c:if test="${oneDay == day}">
									<option value="${oneDay}" selected>${oneDay}日</option>
								</c:if>
								<c:if test="${oneDay != day}">
									<option value="${oneDay}">${oneDay}日</option>
								</c:if>
							</c:forEach>
						</select>
						</td>
					</tr>
					<tr>
						<td nowrap>時刻</td>
						<td>
						<select name="startHour" id="startHour">
							<option value="99" selected>--時</option>
							<c:forEach items="${hourList}" var="hour">
								<c:if test="${hour == startHour}">
									<option value="${hour}" selected>${hour}時</option>
								</c:if>
								<c:if test="${hour != startHour}">
									<option value="${hour}" >${hour}時</option>
								</c:if>
							</c:forEach>
						</select>
						<select name="startMinute" id="startMinute">
							<option value="0">00分</option>
							<option value="30">30分</option>
						</select> ～
						<select name="endHour" id="endHour">
							<option value="99" selected>--時</option>
							<c:forEach items="${hourList}" var="hour">
								<option value="${hour}">${hour}時</option>
							</c:forEach>
						</select>
						<select name="endMinute" id="endMinute">
							<option value="0">00分</option>
							<option value="30">30分</option>
						</select>
						</td>
					</tr>
					<tr>
						<td nowrap>予定</td>
						<td><input type="text" name="plan" value="" size="30" maxlength="100px" required></td>
					</tr>
					<tr>
						<td valign="top" nowrap>メモ</td>
						<td><textarea name="memo" cols="30" rows="10" wrap="virtual" ></textarea></td>
					</tr>
				</table>
				<p>
				<input type="submit" name="register" value="登録する">
				<input type="reset" value="入力しなおす">
				<p>
			</form>
		</div>
	</div>

	<script type="text/javascript">
	function message(){
		var startHour = parseInt($('#startHour').val());
		var endHour = parseInt($('#endHour').val());
		var startMinute = $('#startMinute').val();
		var endMinute = $('#endMinute').val();
		if(startHour == "99" || endHour == "99"){
			window.alert('時刻が適切に埋まっていません');
			return false;
		}else if((startHour > endHour) || ((startHour == endHour) && (startMinute == 30))){
			window.alert('時刻の設定が不適切です');
			return false;
		}
	}
	</script>
</body>
</html>