<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" Content="text/html;charset=Shift_JIS">
<title>スケジュール管理</title>
<link href="./css/top.css" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
</head>
<body>
	<form action="./top" method="post" style="display: inline-block; _display: inline">
		<input type="hidden" name="lastMonth" value="${month}">
		<input type="hidden" name="year" value="${year}">
		<input type="submit" value="先月" class="submit" />
	</form>
	<p style="display: inline-block; _display: inline">${year}年${month}月</p>
	<form action="./top" method="post" style="display: inline-block; _display: inline">
		<input type="hidden" name="nextMonth" value="${month}">
		<input type="hidden" name="year" value="${year}">
		<input type="submit" value="来月" class="submit" />
	</form>
	<table>
		<tr>
			<td class="week" style="color:red;">日</td>
			<td class="week">月</td>
			<td class="week">火</td>
			<td class="week">水</td>
			<td class="week">木</td>
			<td class="week">金</td>
			<td class="week" style="color:blue;">土</td>
		</tr>
		<c:forEach items="${allWeek}" var="oneWeek">
			<tr>
				<c:forEach items="${oneWeek}" var="oneDay">
					<td class="day">${oneDay.day}</td>
				</c:forEach>
			</tr>
			<tr>
				<c:forEach items="${oneWeek}" var="oneDay">
					<td class="sche">
						<c:if test="${oneDay.thisMonth}">
							<form action="./schedule" method="get">
								<input type="hidden" name="year" value="${year}">
								<input type="hidden" name="month" value="${month}">
								<input type="hidden" name="day" value="${oneDay.day}" id="day">
								<input type="image" src="./img/schedule.png" width="14" height="16" />
							</form>
							<c:forEach items="${schedules}" var="schedule">
								<c:if test="${schedule.day == oneDay.day}">
									<div class="contents" >${schedule.startTime } - ${schedule.endTime }
									<a href="./setting?scheduleId=${schedule.id}">${schedule.schedule}</a></div>
								</c:if>
							</c:forEach>

						</c:if>
					</td>
				</c:forEach>
		</c:forEach>
	</table>

</body>
</html>